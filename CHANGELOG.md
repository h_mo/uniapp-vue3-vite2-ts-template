# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## 2.0.1 (2024-08-30)


### ✨ Features | 新功能

* Added utility function - judgePlatform ([e28a64a](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e28a64a6939583695722a6dffcd22660625c1069))
* duotone图标样式配置 ([998c87f](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/998c87fa76e96a00bb60477546b702cefc4c6438))
* FontAwesomeIcon ([89a695f](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/89a695fbde7ebc79c0ba57c6cb84376c84d55db0))
* FontAwesomeIcon click emit ([f8866ec](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f8866ece2a741819b16c3f4448d4ae18f2db7909))
* FontAwesomeIcon counter ([6de5d36](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6de5d36126c49fbab5c508b146050feabce69fcf))
* FontAwesomeIcon demo example ([460387c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/460387ca9329d5c8da27b9c3ca839f5a116eacd3))
* FontAwesomeIcon stack example ([e587fb5](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e587fb5be6c9ff73603442f7994e69bc13c3a3cc))
* FontAwesomeIcon 兼容性 ([3a331d7](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/3a331d73e30bc9db23892cf98515b03e08f56cc5))
* mock数据 ([a7c3606](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/a7c36064f58f4b618d1c699d0f1c939759dde63e))
* **readme:** 更新 Vite 和 ESLint 版本号，调整测试通过平台说明 ([51dc1e6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/51dc1e69dbaf58bd233c7c259a6a0a5cc1ef04ca))
* **release:** 添加版本发布脚本和更新类型定义 ([cf2cadd](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/cf2caddefb9b27dcc9f368a5919f0193eb23c2ad))
* tabBar页面登录优化 ([825db53](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/825db5362af05a56c3aecf1b8f4ac9de5542d4a7))
* tabBar页面登录优化 ([62d62ed](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/62d62ede02d2cf40ef11b4613b6324754d7957e2))
* useRoute and useRouter Hook ([b672549](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b672549cff08a63e491f987dd5e0e601ef9de2e4))
* **vite-config:** 添加页面配置文件至重启监听列表 ([1e7a1cc](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/1e7a1cc371d14b99bb510fe260fd24b25756c685))
* 使用uni-mini-router路由管理 ([71c0de1](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/71c0de151c85c8885952abecffcf6948aa7dcaec))
* 兼容多端统一配置BASE_URL环境变量 ([6ca18b4](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6ca18b4bc2af917e9484ed3fcb1c3ad30c24eaa3))
* 在生产环境中排除 mock 数据 ([0598f3a](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0598f3a09def3bf3d1dd6e7076c929c631df7fcc))
* 增加 iconify 组件，可任意使用全网图标 ([f6b06d5](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f6b06d5a612c181046384e3bb15e390c0d342504))
* 增加commitlint提交方式 ([ec1d011](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ec1d011a7788184e45740e7f2453aa68be4580c9))
* 完善FontAwesomeIcon demo ([7b9a1e8](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7b9a1e81f2710a56d6961cd18721fc41136741d9))
* 完善部分代码 ([862ee17](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/862ee17c88b187812575ee73da727f2ce0d803bd))
* 完整显示图标 ([b60f7fa](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b60f7fa728ec9e833c03cf8a41c51a9469d1fc2a))
* 引入 `uni-vue-devtools`，并配置 `dev:mp-weixin` 自动启动 ([fd44193](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/fd4419389491c6bb1efbea32e0cb3ffd07e8a5c2))
* 当前platform(平台) ([3a83e5b](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/3a83e5b4fbcc9a4eb566873ce0bd7b9e27f7b530))
* 改用alova请求 ([bb83550](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/bb83550fe19867dfc7c9843068ec6b6676ab9e29))
* 新增 VITE_USE_MOCK 配置 ([ac3b3ad](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ac3b3adec77e1af020dab4dfd4ceb02b894d8745))
* 新增[unplugin-auto-import] 自动导入 ([26f1911](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/26f1911ff284176abc4ce08c9dc024ac15bd4a04))
* 新增设置系统剪贴板的内容工具函数 ([b7ccd95](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b7ccd95e9b7be90ccc8992bd14c34eaedc7c162e))


### 🐛 Bug Fixes | Bug 修复

* [H5]:运行环境判断错误 ([0b3f560](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0b3f5601c85eecb2f2a76a805421242bc0523014))
* **http:** 修复 http 配置错误 ([b3a0ea7](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b3a0ea73af3a9414fbbfb136176369e9d1edcfb2))
* **Iconify:** 修复在父组件使用 Iconify 组件时，tailwindcss 的 class无效的问题 ([b9f433a](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b9f433a9e5628aba4bd0af8dc7f47ebf937e8f38))
* 修复 eslint 扁平化配置错误 ([b26db07](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b26db07d1b8dae25c70cd6370ffcb3314093b725))
* 修复在小程序中获取当前平台使用 process.env['UNI_PLATFORM'] 无效问题 ([ff4b1f9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ff4b1f9278ffa5ff31a1aee278bf2404c3f190ba))
* 修复文件路径大小写手误告警 ([9748797](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/97487974c2cc2058779fb7a5824b158e7b600da7))
* 修复没有正确配置 eslint 扁平化配置的问题 ([81d1b6c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/81d1b6c707f498d5bb93f73b3d6a1484e54d2703))
* 修复跳转到登录页面传参错误 ([b5f7e31](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b5f7e31b706cf7cc2b0694cf2e0d5ff63ae7ca5e))
* 修改 AppProvider 组件的文件名为 index.vue ([6cad0d4](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6cad0d4370b1cde33b867dc9b88bba1e443eb28b))
* 导入 'uni-mini-router' 类型不正确问题 ([f997a9d](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f997a9d6c1312b10f69d56cd4708b8625ea174b0))
* 开发环境 h5 代理配置 ([8cc0fc9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/8cc0fc91f41f27912c33dedcb3e444f389c2ce65))
* 忽略 *.local 本地配置文件 ([58b83be](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/58b83be0af122a5bc8be0cfd93f228310b6f6dd8))
* 移动 env.d.ts 到 types 目录 ([03bc0d8](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/03bc0d8b22523221101397121f5bc9f7f9017545))
* 移除无关项 ([e3c3759](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e3c375954804344bc983962f0a28b157c16ef917))
* 解决无模块 'uview-plus' 问题，并扩展 uni.$u 类型支持 ([0a0f4ce](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0a0f4cea13fa83e217a79adc7f34dda620da7662))


### 💄 Styles | 风格

* eslint 格式化代码 ([0fea321](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0fea321256c3dd082d490c340d3e81d6e745ceca))
* manifest.json ([c064617](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/c064617c164aecf19064d2de42d0ccbe3c27f6f5))
* remove console.log ([095dfbb](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/095dfbbc6f126252b7482c3cedace51f14dc66fb))
* 优化路由跳转 ([f7a5f35](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f7a5f350f7b9dd54ab9e478d2fa5f68e8d414a0f))
* 优化路由跳转 ([43786d6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/43786d6672dda47c6c3c196a7c350858b9fc8cff))
* 换行改为CRLF ([7e141b6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7e141b6e54ed976257224d903ed5db04c1edd1fb))
* 更改cache文件夹名 ([4d62958](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/4d62958316a7895d4492f0d991f746aacd8935a5))
* 格式化 ([002325b](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/002325bfbb5a24eb9cc94364c748451fe1aaff60))


### 💄 Refactor | 重构

* **build:** 简化代理配置并更新HTTP错误处理重构代理配置，提取到proxy.ts中以简化vite.config.ts中的代码。同时，修改了faultTolerance.ts中的HTTP错误处理，使用具体的ResultEnum替代类型导入，以提高代码的准确性和可读性。 ([79240a2](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/79240a29f847d37f76f3bb6fdf526396d5806fe4))
* **components:** remove unused BasicInput, Iconify, and Test componentsRemove the unused BasicInput, Iconify, and Test components to clean up the codebase. ([743a021](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/743a0219b31aa47810e23c8e66527930a46ec158))
* **http:** 升级alova到v3, 优化状态码和逻辑错误处理 ([5a705f9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/5a705f9a44bcb105a8621b2e42e727ba2ca715d9))
* **iconify:** 更新图标组件并调整样式 ([26a8bfb](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/26a8bfba42171cec9cef36ddfa24f950de4a20eb))
* Request使用依赖 [@luch-request](https://gitee.com/luch-request) ([b1ec215](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b1ec2157d789f5618528bf0257e276b3a8f7ec9a))
* tailwindcss 替换 unocss ([09e4093](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/09e4093acaeb8116427bfee595d5b418fc55b13a))


### ⚡ Performance Improvements | 性能优化

* httpRequest 优化 ([d6b7132](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/d6b71321b560570cb493799cc38ede98cbf958fe))
* 优化登录体验 ([d64ee1c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/d64ee1cb80d8e358c0a54904b6819ec86f832b69))
* 优化部分代码 ([421ec1c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/421ec1cb3da54f64562743f6518c9cbb2e28936f))
* 使用组合式 store ([a772075](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/a772075200373aed024d3f5c97dbbd2eadd87ba6))
* 注释logout api 的调用 ([7bc5b4c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7bc5b4ccbaae3316522cf083fb3ea4cea4718064))
* 获取Platform（平台）优化 ([8232791](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/8232791a58cfd8bb8bb54a86d4c8ba152d40b37f))
* 路由优化 ([4057671](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/4057671444221edc7286e2b122b5c0f941a2f8d7))
* 路由优化 ([cd50917](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/cd50917754c29ccac5ed95edee629c732c534d5f))


## 2.0.0 (2024-08-20)


### ✨ Features | 新功能

* 当前platform(平台) ([3a83e5b](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/3a83e5b4fbcc9a4eb566873ce0bd7b9e27f7b530))
* 改用alova请求 ([bb83550](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/bb83550fe19867dfc7c9843068ec6b6676ab9e29))
* 兼容多端统一配置BASE_URL环境变量 ([6ca18b4](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6ca18b4bc2af917e9484ed3fcb1c3ad30c24eaa3))
* 使用uni-mini-router路由管理 ([71c0de1](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/71c0de151c85c8885952abecffcf6948aa7dcaec))
* 完善部分代码 ([862ee17](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/862ee17c88b187812575ee73da727f2ce0d803bd))
* 完善FontAwesomeIcon demo ([7b9a1e8](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7b9a1e81f2710a56d6961cd18721fc41136741d9))
* 完整显示图标 ([b60f7fa](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b60f7fa728ec9e833c03cf8a41c51a9469d1fc2a))
* 新增 VITE_USE_MOCK 配置 ([ac3b3ad](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ac3b3adec77e1af020dab4dfd4ceb02b894d8745))
* 新增[unplugin-auto-import] 自动导入 ([26f1911](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/26f1911ff284176abc4ce08c9dc024ac15bd4a04))
* 新增设置系统剪贴板的内容工具函数 ([b7ccd95](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b7ccd95e9b7be90ccc8992bd14c34eaedc7c162e))
* 引入 `uni-vue-devtools`，并配置 `dev:mp-weixin` 自动启动 ([fd44193](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/fd4419389491c6bb1efbea32e0cb3ffd07e8a5c2))
* 在生产环境中排除 mock 数据 ([0598f3a](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0598f3a09def3bf3d1dd6e7076c929c631df7fcc))
* 增加 iconify 组件，可任意使用全网图标 ([f6b06d5](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f6b06d5a612c181046384e3bb15e390c0d342504))
* 增加commitlint提交方式 ([ec1d011](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ec1d011a7788184e45740e7f2453aa68be4580c9))
* Added utility function - judgePlatform ([e28a64a](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e28a64a6939583695722a6dffcd22660625c1069))
* duotone图标样式配置 ([998c87f](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/998c87fa76e96a00bb60477546b702cefc4c6438))
* FontAwesomeIcon ([89a695f](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/89a695fbde7ebc79c0ba57c6cb84376c84d55db0))
* FontAwesomeIcon 兼容性 ([3a331d7](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/3a331d73e30bc9db23892cf98515b03e08f56cc5))
* FontAwesomeIcon click emit ([f8866ec](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f8866ece2a741819b16c3f4448d4ae18f2db7909))
* FontAwesomeIcon counter ([6de5d36](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6de5d36126c49fbab5c508b146050feabce69fcf))
* FontAwesomeIcon demo example ([460387c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/460387ca9329d5c8da27b9c3ca839f5a116eacd3))
* FontAwesomeIcon stack example ([e587fb5](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e587fb5be6c9ff73603442f7994e69bc13c3a3cc))
* mock数据 ([a7c3606](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/a7c36064f58f4b618d1c699d0f1c939759dde63e))
* **readme:** 更新 Vite 和 ESLint 版本号，调整测试通过平台说明 ([51dc1e6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/51dc1e69dbaf58bd233c7c259a6a0a5cc1ef04ca))
* **release:** 添加版本发布脚本和更新类型定义 ([cf2cadd](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/cf2caddefb9b27dcc9f368a5919f0193eb23c2ad))
* tabBar页面登录优化 ([825db53](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/825db5362af05a56c3aecf1b8f4ac9de5542d4a7))
* tabBar页面登录优化 ([62d62ed](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/62d62ede02d2cf40ef11b4613b6324754d7957e2))
* useRoute and useRouter Hook ([b672549](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b672549cff08a63e491f987dd5e0e601ef9de2e4))
* **vite-config:** 添加页面配置文件至重启监听列表 ([1e7a1cc](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/1e7a1cc371d14b99bb510fe260fd24b25756c685))


### 🐛 Bug Fixes | Bug 修复

* [H5]:运行环境判断错误 ([0b3f560](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0b3f5601c85eecb2f2a76a805421242bc0523014))
* 导入 'uni-mini-router' 类型不正确问题 ([f997a9d](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f997a9d6c1312b10f69d56cd4708b8625ea174b0))
* 忽略 *.local 本地配置文件 ([58b83be](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/58b83be0af122a5bc8be0cfd93f228310b6f6dd8))
* 解决无模块 'uview-plus' 问题，并扩展 uni.$u 类型支持 ([0a0f4ce](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0a0f4cea13fa83e217a79adc7f34dda620da7662))
* 开发环境 h5 代理配置 ([8cc0fc9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/8cc0fc91f41f27912c33dedcb3e444f389c2ce65))
* 修复 eslint 扁平化配置错误 ([b26db07](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b26db07d1b8dae25c70cd6370ffcb3314093b725))
* 修复没有正确配置 eslint 扁平化配置的问题 ([81d1b6c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/81d1b6c707f498d5bb93f73b3d6a1484e54d2703))
* 修复跳转到登录页面传参错误 ([b5f7e31](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b5f7e31b706cf7cc2b0694cf2e0d5ff63ae7ca5e))
* 修复文件路径大小写手误告警 ([9748797](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/97487974c2cc2058779fb7a5824b158e7b600da7))
* 修复在小程序中获取当前平台使用 process.env['UNI_PLATFORM'] 无效问题 ([ff4b1f9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/ff4b1f9278ffa5ff31a1aee278bf2404c3f190ba))
* 修改 AppProvider 组件的文件名为 index.vue ([6cad0d4](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/6cad0d4370b1cde33b867dc9b88bba1e443eb28b))
* 移除无关项 ([e3c3759](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/e3c375954804344bc983962f0a28b157c16ef917))
* 移动 env.d.ts 到 types 目录 ([03bc0d8](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/03bc0d8b22523221101397121f5bc9f7f9017545))


### 💄 Styles | 风格

* 格式化 ([002325b](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/002325bfbb5a24eb9cc94364c748451fe1aaff60))
* 更改cache文件夹名 ([4d62958](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/4d62958316a7895d4492f0d991f746aacd8935a5))
* 换行改为CRLF ([7e141b6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7e141b6e54ed976257224d903ed5db04c1edd1fb))
* 优化路由跳转 ([f7a5f35](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/f7a5f350f7b9dd54ab9e478d2fa5f68e8d414a0f))
* 优化路由跳转 ([43786d6](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/43786d6672dda47c6c3c196a7c350858b9fc8cff))
* eslint 格式化代码 ([0fea321](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/0fea321256c3dd082d490c340d3e81d6e745ceca))
* manifest.json ([c064617](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/c064617c164aecf19064d2de42d0ccbe3c27f6f5))
* remove console.log ([095dfbb](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/095dfbbc6f126252b7482c3cedace51f14dc66fb))


### 💄 Refactor | 重构

* **build:** 简化代理配置并更新HTTP错误处理重构代理配置，提取到proxy.ts中以简化vite.config.ts中的代码。同时，修改了faultTolerance.ts中的HTTP错误处理，使用具体的ResultEnum替代类型导入，以提高代码的准确性和可读性。 ([79240a2](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/79240a29f847d37f76f3bb6fdf526396d5806fe4))
* **components:** remove unused BasicInput, Iconify, and Test componentsRemove the unused BasicInput, Iconify, and Test components to clean up the codebase. ([743a021](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/743a0219b31aa47810e23c8e66527930a46ec158))
* **http:** 升级alova到v3, 优化状态码和逻辑错误处理 ([5a705f9](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/5a705f9a44bcb105a8621b2e42e727ba2ca715d9))
* **iconify:** 更新图标组件并调整样式 ([26a8bfb](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/26a8bfba42171cec9cef36ddfa24f950de4a20eb))
* Request使用依赖 [@luch-request](https://gitee.com/luch-request) ([b1ec215](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/b1ec2157d789f5618528bf0257e276b3a8f7ec9a))
* tailwindcss 替换 unocss ([09e4093](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/09e4093acaeb8116427bfee595d5b418fc55b13a))


### ⚡ Performance Improvements | 性能优化

* 获取Platform（平台）优化 ([8232791](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/8232791a58cfd8bb8bb54a86d4c8ba152d40b37f))
* 路由优化 ([4057671](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/4057671444221edc7286e2b122b5c0f941a2f8d7))
* 路由优化 ([cd50917](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/cd50917754c29ccac5ed95edee629c732c534d5f))
* 使用组合式 store ([a772075](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/a772075200373aed024d3f5c97dbbd2eadd87ba6))
* 优化部分代码 ([421ec1c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/421ec1cb3da54f64562743f6518c9cbb2e28936f))
* 优化登录体验 ([d64ee1c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/d64ee1cb80d8e358c0a54904b6819ec86f832b69))
* 注释logout api 的调用 ([7bc5b4c](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/7bc5b4ccbaae3316522cf083fb3ea4cea4718064))
* httpRequest 优化 ([d6b7132](https://gitee.com/h_mo/uniapp-vue3-vite-ts-template/commit/d6b71321b560570cb493799cc38ede98cbf958fe))
